#!/usr/bin/env python3
from os import path
from argparse import ArgumentParser, FileType, ArgumentDefaultsHelpFormatter
from benchmarktk import Benchmark, BenchmarkError, yaml_load_all


class GenerateStats(Benchmark):

    def get_cmd(self, cmd):
        dirname = self.get('stats-tk', '')
        if dirname != "":
            return path.join(dirname, cmd)
        return cmd

    def generate_stats_for(self, version, params):
        params = dict(params)
        params['version'] = version
        # Input file
        in_file = self['version_data_format']
        in_file = self.format(in_file, params)
        # Output file
        out_file = self['version_stats_format']
        out_file = self.format(out_file, params)
        stk_sample = self.get_cmd('stk-sample')
        stk_conf = self.get_cmd('stk-confidence')
        mk_sample = self.get_cmd('benchmarktk-sample')
        cmd = f'{mk_sample} {in_file} | {stk_sample} - -o - | {stk_conf} - -o {out_file}'
        self.safe_exec(cmd)
        return out_file

    def get_benchmark_logs(self):
        for b in self.get_benchmarks():
            for params in self.get_parameters(b):
                params['benchmark'] = b
                for version in self.get_versions():
                    yield version, params

    def __call__(self):
        for version, params in self.get_benchmark_logs():
            self.generate_stats_for(version, params)


def main() -> None:
    parser = ArgumentParser(description='Parses the logs and generates the stats.',
                            formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument('files', metavar='S', type=FileType('r'), nargs="+", help="The benchmark configuration file.")
    r = parser.parse_args()
    opts = yaml_load_all(*r.files)
    try:
        c = GenerateStats(opts)
        c()
    except BenchmarkError as e:
        import sys
        print(e, file=sys.stderr)
        sys.exit(1)


if __name__ == '__main__':
    main()
