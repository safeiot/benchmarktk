#!/usr/bin/env python3
import json
from os import path
from pathlib import Path
from argparse import ArgumentParser, FileType, ArgumentDefaultsHelpFormatter
from benchmarktk import Benchmark, BenchmarkError, yaml_load_all


class Collect(Benchmark):

    def get_cmd(self, cmd):
        dirname = self.get('stats-tk', '')
        if dirname != "":
            return path.join(dirname, cmd)
        return cmd

    def get_logs(self, version, params):
        fmt = self['log_format']
        match = path.basename(fmt)
        params = dict(params)  # copy
        for lap in self.get_run_range():
            params['run'] = lap
            yield fmt.format(**params)

    def parse_log(self, fname, params):
        if not Path(fname).exists():
            raise RuntimeError(f'File not found: {fname}')
        version = params['version']
        cmd = self[version]['parse_cmd']
        params = dict(params)
        params['log'] = fname
        cmd = self.format(cmd, params).strip()
        return self.safe_exec_output(cmd)

    def hash_log(self, fname):
        data = self.safe_exec_output(f"md5sum {fname}")
        if data is not None:
            for line in data.split("\n"):
                line = line.strip()
                if line != "":
                    return line.split(" ")[0]

    def parse_version_logs(self, version, params):
        params = dict(params)
        params['version'] = version
        out_file = self['version_data_format']
        out_file = self.format(out_file, params)
        try:
            with open(out_file, 'rb') as fp:
                data = json.load(fp)
        except (OSError, json.decoder.JSONDecodeError):
            data = dict()
        for fname in self.get_logs(version, params):
            parsed = data.get(fname, None)
            if parsed is None or not isinstance(parsed, dict):
                output = self.parse_log(fname, params)
                input_hash = self.hash_log(fname)
                data[fname] = {"input_hash": input_hash, "data": output, 'hash_function': 'md5'}
            else:
                input_hash = self.hash_log(fname)
                if input_hash != parsed.get('input_hash', None):
                    output = self.parse_log(fname, params)
                    data[fname] = {"input_hash": input_hash, "data": output, 'hash_function': 'md5'}
        if self.is_dry_run():
            return

        with open(out_file, 'w') as fp:
            json.dump(data, fp)
        return data

    def gen_sample(self, version, params):
        params = dict(params)
        params['version'] = version
        out_file = self['version_stats_format']
        out_file = self.format(out_file, params)

    def parse_logs(self, params):
        versions = self.get_versions()
        if len(versions) == 1:
            checked, = versions
            checked_fn = self.parse_version_logs(checked, params)
            return

        if len(versions) != 2:
            raise Exception("Can only compare two versions, got: %r" % versions)
        checked, origin = versions
        checked_fn = self.generate_version_mean(checked, params)
        origin_fn = self.generate_version_mean(origin, params)
        filename = self['diffs_data_format']
        params['version1'] = checked
        params['version2'] = origin

        dst_file = self.format(filename, params)

        cmd = self.get_cmd('stk-compare')
        cmd = "%s %s %s -o %s" % (cmd, checked_fn, origin_fn, dst_file)
        self.safe_exec(cmd)

    def collect_data(self):
        for b in self.get_benchmarks():
            for params in self.get_parameters(b):
                params['benchmark'] = b
                self.parse_logs(params)


def main() -> None:
    parser = ArgumentParser(description='Parses the logs and generates the stats.',
                            formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument('files', metavar='FILE', type=FileType('r'), nargs="+",
                        help="The benchmark configuration file.")
    r = parser.parse_args()
    opts = yaml_load_all(*r.files)
    try:
        c = Collect(opts)
        c.collect_data()
    except (OSError, BenchmarkError) as e:
        import sys

        print(e, file=sys.stderr)
        sys.exit(1)


if __name__ == '__main__':
    main()
