#!/usr/bin/env python3
from pathlib import Path
from argparse import ArgumentParser, FileType
from benchmarktk import Benchmark, BenchmarkError, yaml_load_all


class RunBenchmark(Benchmark):
    def get_version(self, version, key):
        try:
            return self[version][key]
        except KeyError:
            return self[key]

    def get_run_command(self, version):
        return self[version]['run_cmd'].strip()

    def get_exec_retries(self, version):
        try:
            return int(self.get_version(version, 'exec_retries'))
        except KeyError:
            return 5
        except ValueError:
            msg = "Expecting an integer in key '%s.exec_retries'. Got: %s"
            msg %= version, self[version].get('exec_retries', "")
            raise RuntimeError(msg)

    def use_resume(self):
        return self.get('resume', False)

    def run_lap(self, params, log=None, run=0, warm_up=False):
        """
        Runs the benchmark once.
        """
        if warm_up:
            run = 0
            log = "/dev/null"
        params['log'] = log
        params['run'] = run
        cmd = self.get_run_command(params['version'])
        try:
            cmd = cmd.format(**params)
        except KeyError as e:
            raise ValueError("key=%r params=%r format=%r" % (e.args[0], params, cmd))
        # Check resume
        if self.use_resume() and Path(log).exists() and run > 0:
            # File exists, no need to re-run it
            self.print_warning(f"Skip: {log}")
            return
        # The program did not resume, check if we need to warm up
        if self.warm_up:
            self.warm_up = False
            self.run_lap(params, warm_up=True)
        # Warm-up is done, now run the program
        needs_retry = True
        exec_retries = self.exec_retries
        while needs_retry and exec_retries >= 0:
            if exec_retries == 0:
                self.safe_exec(cmd)
                needs_retry = False
            elif self.system(cmd) != 0:
                self.print_warning("RETRY EXECUTING: " + cmd)
                exec_retries -= 1
            else:
                needs_retry = False

    def run_series(self, params):
        """
        Runs the benchmark on a full series.
        """
        # Enable warm-up
        self.warm_up = True
        self.exec_retries = self.get_exec_retries(params['version'])
        assert self.exec_retries >= 0
        fmt = self.opts['log_format']
        run_count = self.get_run_count()
        for r in self.get_run_range():
            kv = dict(params)
            kv['run'] = r
            log = fmt.format(**kv)
            self.run_lap(params=params, log=log, run=r)

    def full_run(self):
        for b in self.get_benchmarks():
            for params in self.get_parameters(b):
                for version in self.get_versions():
                    params['benchmark'] = b
                    params['version'] = version
                    self.run_series(params)


def main() -> None:
    parser = ArgumentParser(description='Run the benchmarks.')
    parser.add_argument('files', metavar='FILE', type=FileType('r'), nargs="+",
                        help="The benchmark configuration file.")
    r = parser.parse_args()
    try:
        opts = yaml_load_all(*r.files)
        bench = RunBenchmark(opts)
        bench.full_run()
    except BenchmarkError as e:
        import sys
        print(e, file=sys.stderr)
        sys.exit(1)


if __name__ == '__main__':
    main()
